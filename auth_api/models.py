'''Imports'''
from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class UserType(models.Model):
    '''Model for User types.'''
    name = models.TextField()

class AmsUser(AbstractUser):
    '''Model for Custom User'''
    ams_user_type = models.ManyToManyField(UserType, related_name='ams_user_type')
