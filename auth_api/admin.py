'''Models for admin'''
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import AmsUser

# Register your models here.
class AmsUserAdmin(UserAdmin):
    '''Model for Ams User Admin'''
    model = AmsUser

admin.site.register(AmsUser, AmsUserAdmin)
