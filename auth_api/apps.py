
'''Imports'''
from django.apps import AppConfig


class AuthApiConfig(AppConfig):
    '''Config for AuthApi app'''
    name = 'auth_api'
