'''Imports'''
from rest_framework.test import APITestCase
from rest_framework import status

# Create your tests here.
class AuthApiTestCase(APITestCase):
    '''Test case for auth_api'''
    def setUp(self):
        self.client.post('/auth/users/', data={
            'username':  'admin',
            'password':  'Testing123!!!'
        }, format='json')
        self.token = ''

    def login(self):
        '''Start auth'''
        self.token = ''
        response = self.client.post(
            '/auth/jwt/create/',
            data={'username':  'admin', 'password':  'Testing123!!!'},
            format='json'
        )
        self.token = response.data['access']
        return response

    def test_login(self):
        '''Test for login'''
        response = self.login()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_verify(self):
        '''Verify JWT Token'''
        self.login()
        response = self.client.post(
            '/auth/jwt/verify',
            data={'token':  self.token},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
